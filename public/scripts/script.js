alert("Grę dedykuję dawnym, szkolnym dniom... Udanej zabawy! :)");
const loader=function(){
	let number=Math.floor(Math.random()*7+1);
	document.body.style.backgroundImage='url("./wallpapers/pk'+number+'.png")';
};
const theme=new Audio("./sounds/theme.mp3");
theme.loop=true;
window.onload=loader;

Vue.component("banner",{
	props:{
		password_hidden:{
			type:String,
			required:true,
		},
	},
	template:`<div id="banner">{{password_hidden}}</div>`,
});

Vue.component("poster",{
	props:{
		fails:{
			type:Number,
			required:!null,
		},
	},
	template:`
		<div id="poster">
			<div style='height:40px;'></div>
			<img :src="url" :alt="error">
		</div>`,
	data(){
		return{
			error:"File not found",
		}
	},
	computed:{
		url:function(){
			return "./images/s"+this.fails+".jpg";
		},
	},
});

Vue.component("panel",{
	template:`
		<div id="panel">
			<div v-for="symbol in symbols" class='symbol' :id="symbol" @mouseover="hover(symbol)" @click="click(symbol)">{{symbol}}</div>
			<div style="clear:both;"></div>
			<div v-for="symbol in symbols1" class='symbol' :id="symbol" @mouseover="hover(symbol)" @click="click(symbol)">{{symbol}}</div>
			<div style="clear:both;"></div>
			<div v-for="symbol in symbols2" class='symbol' :id="symbol" @mouseover="hover(symbol)" @click="click(symbol)">{{symbol}}</div>
			<div style="clear:both;"></div>
			<div v-for="symbol in symbols3" class='symbol' :id="symbol" @mouseover="hover(symbol)" @click="click(symbol)">{{symbol}}</div>
			<div style="clear:both;"></div>
			<div v-for="symbol in symbols4" class='symbol' :id="symbol" @mouseover="hover(symbol)" @click="click(symbol)">{{symbol}}</div>
			<div style="clear:both;"></div>
			<div v-for="symbol in symbols5" class='symbol' :id="symbol" @mouseover="hover(symbol)" @click="click(symbol)">{{symbol}}</div>
			<div style="clear:both;"></div>
			<div v-for="symbol in symbols6" class='symbol' :id="symbol" @mouseover="hover(symbol)" @click="click(symbol)">{{symbol}}</div>
			<div style="clear:both;"></div>
		</div>
	`,
	data(){
		return{
			symbols:["A","Ą","B","C","Ć","D","E"],
			symbols1:["Ę","F","G","H","I","J","K"],
			symbols2:["L","Ł","M","N","Ń","O","Ó"],
			symbols3:["P","Q","R","S","Ś","T","U"],
			symbols4:["V","W","X","Y","Z","Ź","Ż"],
			symbols5:["1","2","3","4","5","6","7"],
			symbols6:["8","9","♀","♂","'","-","."],
		}
	},
	methods:{
		hover:function(symbol){
			if(document.getElementById(symbol).className=="symbol"){
				const sound=new Audio("./sounds/yes.wav");
				sound.play();
			};
		},
		click:function(symbol){
			if(document.getElementById(symbol).className=="symbol")
			this.$emit("clicked",symbol);
		},
	},
});

Vue.component("choice_panel",{
	props:{
		attempt:{
			type:Number,
			required:!null,
		},
	},
	template:`
		<div id='bannerWyboru' style='text-align:center;'>
			<h4>Wybierz generację pokemonów, z której chcesz odgadywać hasło. :-)</h4>
			<div style='padding-left:235px;'>
				<div class='symbol' @click='modifier("first")' @mouseover="button_hovered()" style='width:130px;height:68px;'>I generacja</div>
				<div class='symbol' @click='modifier("second")' @mouseover="button_hovered()" style='width:130px;height:68px;'>II generacja</div>
				<div class='symbol' @click='modifier("both")' @mouseover="button_hovered()" style='width:130px;height:68px;'>I i II generacja</div>
			</div>
		</div>
	`,
	data(){
		return{
			flag:true,
			gen:null,
		}
	},
	methods:{
		button_hovered:function(){
			this.$emit("hovered");
		},
		modifier:function(variable){
			this.flag=false;
			this.gen=variable;
			let num=Math.floor(Math.random()*5+1);
			let track="./sounds/pika"+num+".mp3";
			const sound=new Audio(track);
			sound.play();
			this.$emit("update",[this.flag,this.gen]);
			if(this.attempt==1){
				theme.play();
			};
		},
	},
});

Vue.component("mute_button",{
	props:{
		attempt:{
			type:Number,
			required:!null,
		},
	},
	template:`
		<div id='button_border'>
			<div style='padding-left:355px;'>
				<div v-if="attempt>0" class="buttons" @click="mutedModifier()" @mouseover="button_hovered()">Włącz/wyłącz muzykę</div>
			</div>
		</div>
	`,
	data(){
		return{
			muted:false,
		}
	},
	methods:{
		mutedModifier:function(){
			if(this.muted==false){
				theme.pause();
				this.muted=true;
			}
			else{
				theme.play();
				this.muted=false;
			};
		},
		button_hovered:function(){
			this.$emit("hovered");
		},
	},
});

const myApp=new Vue({
	el:"#app",
	template:`
		<div id='container' :key='attempt'>
			<span v-if="scope_choice">
				<choice_panel @update='updater' :attempt="attempt" @hovered='hover()'></choice_panel>
				<mute_button :attempt="attempt" v-if="attempt>1" @hovered='hover()'></mute_button>
			</span>
			<span v-else>
				{{set_password()}}
				<banner :password_hidden="password_hidden"></banner>
				<poster :fails="fails"></poster>
				<panel @clicked="check"></panel>
				<mute_button :attempt="attempt" @hovered='hover()'></mute_button>
			</span>
		</div>`,
	data:{
		scope_choice:true,
		flag:true,
		gen:null,
		fails:0,
		password:null,
		password_hidden:null,
		attempt:1,
		pokemon_image:null,
		pokemons:[null,"Bulbasaur","Ivysaur","Venusaur","Charmander","Charmeleon","Charizard","Squirtle","Wartortle","Blastoise","Caterpie","Metapod","Butterfree","Weedle","Kakuna","Beedrill","Pidgey","Pidgeotto","Pidgeot","Rattata","Raticate","Spearow","Fearow","Ekans","Arbok","Pikachu","Raichu","Sandshrew","Sandslash","Nidoran♀","Nidorina","Nidoqueen","Nidoran♂","Nidorino","Nidoking","Clefairy","Clefable","Vulpix","Ninetales","Jigglypuff","Wigglytuff","Zubat","Golbat","Oddish","Gloom","Vileplume","Paras","Parasect","Venonat","Venomoth","Diglett","Dugtrio","Meowth","Persian","Psyduck","Golduck","Mankey","Primeape","Growlithe","Arcanine","Poliwag","Poliwhirl","Poliwrath","Abra","Kadabra","Alakazam","Machop","Machoke","Machamp","Bellsprout","Weepinbell","Victreebel","Tentacool","Tentacruel","Geodude","Graveler","Golem","Ponyta","Rapidash","Slowpoke","Slowbro","Magnemite","Magneton","Farfetch'd","Doduo","Dodrio","Seel","Dewgong","Grimer","Muk","Shellder","Cloyster","Gastly","Haunter","Gengar","Onix","Drowzee","Hypno","Krabby","Kingler","Voltorb","Electrode","Exeggcute","Exeggutor","Cubone","Marowak","Hitmonlee","Hitmonchan","Lickitung","Koffing","Weezing","Rhyhorn","Rhydon","Chansey","Tangela","Kangaskhan","Horsea","Seadra","Goldeen","Seaking","Staryu","Starmie","Mr.Mime","Scyther","Jynx","Electabuzz","Magmar","Pinsir","Tauros","Magikarp","Gyarados","Lapras","Ditto","Eevee","Vaporeon","Jolteon","Flareon","Porygon","Omanyte","Omastar","Kabuto","Kabutops","Aerodactyl","Snorlax","Articuno","Zapdos","Moltres","Dratini","Dragonair","Dragonite","Mewtwo","Mew","Chikorita","Bayleef","Meganium","Cyndaquil","Quilava","Typhlosion","Totodile","Croconaw","Feraligatr","Sentret","Furret","Hoothoot","Noctowl","Ledyba","Ledian","Spinarak","Ariados","Crobat","Chinchou","Lanturn","Pichu","Cleffa","Igglybuff","Togepi","Togetic","Natu","Xatu","Mareep","Flaaffy","Ampharos","Bellossom","Marill","Azumarill","Sudowoodo","Politoed","Hoppip","Skiploom","Jumpluff","Aipom","Sunkern","Sunflora","Yanma","Wooper","Quagsire","Espeon","Umbreon","Murkrow","Slowking","Misdreavus","Unown","Wobbuffet","Girafarig","Pineco","Forretress","Dunsparce","Gligar","Steelix","Snubbull","Granbull","Qwilfish","Scizor","Shuckle","Heracross","Sneasel","Teddiursa","Ursaring","Slugma","Magcargo","Swinub","Piloswine","Corsola","Remoraid","Octillery","Delibird","Mantine","Skarmory","Houndour","Houndoom","Kingdra","Phanpy","Donphan","Porygon2","Stantler","Smeargle","Tyrogue","Hitmontop","Smoochum","Elekid","Magby","Miltank","Blissey","Raikou","Entei","Suicune","Larvitar","Pupitar","Tyranitar","Lugia","Ho-Oh","Celebi"],
	},
	methods:{
		hover:function(){
			const sound=new Audio("./sounds/yes.wav");
			sound.play();
		},
		updater:function(data){
			this.scope_choice=data[0];
			this.gen=data[1];
		},
		set_password:function(){
			if(this.flag){
				this.flag=false;
				let num;
				if(this.gen=="both") num=Math.floor(Math.random()*251+1);
				else if(this.gen=="first") num=Math.floor(Math.random()*151+1);
				else num=Math.floor(Math.random()*100+1)+151;
				this.password=this.pokemons[num].toUpperCase();
				this.hide_password();
				if(num<10) num="00"+num;
				else if(num<100) num="0"+num;
				this.pokemon_image="https://assets.pokemon.com/assets/cms2/img/pokedex/full/"+num+".png";
			};
		},
		hide_password:function(){
			let content="";
			for(i=0;i<this.password.length;++i){
				if(this.password!=" ") content+="_";
				else content+=" ";
			};
			this.password_hidden=content;
		},
		check:function(symbol){
			let content="";
			let array=[];
			let flag=false;
			for(i=0;i<this.password.length;++i){
				array.push(this.password.charAt(i));
			};
			for(i=0;i<this.password.length;++i){
				if(this.password.charAt(i)==symbol){
					flag=true;
					content+=symbol;
				}
				else if(this.password_hidden.charAt(i)=="_") content+="_";
				else content+=array[i];
			};
			if(flag){
				const found=new Audio("./sounds/found.wav");
				found.play();
				document.getElementById(symbol).setAttribute("class","success");
			}
			else{
				const not_found=new Audio("./sounds/no.wav");
				not_found.play();
				document.getElementById(symbol).setAttribute("class","fail");
				this.fails++;
			};
			this.password_hidden=content;
			this.control();
		},
		control:function(){
			if(this.fails==9){
				const loss=new Audio("./sounds/loss.mp3");
				loss.play();
				let content="<span style='color:#993333'>Game over!</span> Niestety, nie udało Ci się odgadnąć hasła: <span style='color:#33cc33;'>"+this.password+"</span>. <span style='font-style:italic'>:(</span><h1 class='reload'>Spróbuj jeszcze raz!</h1>";
				document.getElementById("panel").innerHTML=content;
				document.getElementsByTagName("h1")[0].setAttribute("onclick","myApp.changer()");
			}
			else if(this.password==this.password_hidden){
				let content="<div style='height:40px;'></div><img id='pokemon_image' src="+this.pokemon_image+"><div style='height:40px;'></div>";
				document.getElementById("poster").innerHTML=content;
				const win=new Audio("./sounds/win.mp3");
				setTimeout(function(){win.play()},1000);
				document.getElementById("banner").style.color="#40ff00";
				content="<span style='color:#33cc33'>Gratulacje!</span> Udało Ci się odgadnąć hasło!</span><h1 class='reload'>Grasz dalej? ;-)</h1>";
				document.getElementById("panel").innerHTML=content;
				document.getElementsByTagName("h1")[0].setAttribute("onclick","myApp.changer()");
			};
		},
		changer:function(){
			this.scope_choice=true;
			this.flag=true;
			this.fails=0;
			this.password=null;
			this.password_hidden=null;
			let number=Math.floor(Math.random()*7+1);
			document.body.style.backgroundImage='url("./wallpapers/pk'+number+'.png")';
			this.attempt++;
		},
	},
});